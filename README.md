# Public Minetest Metrics

Prometheus statistics for the public serverlist.

Available at: <https://minetest.rubenwardy.com/metrics>

Also see [ContentDB's metrics](https://content.minetest.net/help/metrics/).

## API

* `server` - include server-specific stats. For example:

		/metrics?server=ctf.rubenwardy.com:30001

	You can combine multiple like so (OR):

		/metrics?server=one&server=two

	The value must consist of either `address` or `address:port`, where address can contain `*` to wildcard match.
