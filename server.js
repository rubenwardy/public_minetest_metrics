
const express = require("express-coroutine")(require("express"));
const got = require("got");
const app = express();
const port = process.env.PORT || 3000;


/**
* Creates a RegExp from the given string, converting asterisks to .* expressions,
* and escaping all other characters.
*/
function wildcardToRegExp (s) {
	return new RegExp("^" + s.split(/\*+/).map(regExpEscape).join(".*") + "$");
}

/**
* RegExp-escapes all characters in the given string.
*/
function regExpEscape(s) {
	return s.replace(/[|\\{}()[\]^$+*?.]/g, "\\$&");
}


function parseServerQuery(query) {
	let address = query;
	let port = null;


	// Get port
	const idx = address.indexOf(":");
	if (idx != -1) {
		port = address.substring(idx + 1, address.length).trim();
		address = address.substring(0, idx);
	}

	return {
		address: address.trim(),
		port: port,
	};
}

function matchServer(server, query) {
	if (query.port && server.port != query.port) {
		return false;
	}

	const re = wildcardToRegExp(query.address);
	return re.test(server.address);
}

async function generateMetrics(server_queries) {
	const response = await got("http://servers.minetest.net/list", { responseType: "json" });

	const prefix = "minetest_serverlist_";

	const stats = [
		{
			"name": "total_clients",
			"type": "gauge",
			"help": "Current number of online clients",
			"value": response.body.total.clients
		},

		{
			"name": "total_servers",
			"type": "gauge",
			"help": "Current number of servers",
			"value": response.body.total.servers
		},

		{
			"name": "max_clients",
			"type": "gauge",
			"help": "Maximum number of observed clients",
			"value": response.body.total_max.clients
		},

		{
			"name": "max_servers",
			"type": "gauge",
			"help": "Maximum number of observed servers",
			"value": response.body.total_max.servers
		},
	]

	let lines = stats.map(stat =>
		`# HELP ${prefix}${stat.name} ${stat.help}\n` +
		`# TYPE ${prefix}${stat.name} ${stat.type}\n` +
		`${prefix}${stat.name} ${stat.value}\n`);

	if (server_queries.length > 0) {
		lines.push(`# HELP ${prefix}rank Position in serverlist`);
		lines.push(`# TYPE ${prefix}rank gauge`);

		server_queries = server_queries.map(query => parseServerQuery(query));

		const servers = response.body.list;
		servers.forEach((server, idx) => server.rank = idx + 1);

		servers
			.filter(server => server_queries.some(query => matchServer(server, query)))
			.forEach(server => {
				const json = JSON.stringify;
				lines.push(`${prefix}rank{address=${json(server.address)},port=${server.port},name=${json(server.name)}} ${server.rank}\n`);
			});
	}
	return lines.join("\n");
}

app.get("/", (_, res) => res.send("Statics are at /metrics"))

app.get("/metrics", async (req, res) => {
	let servers = req.query.server;
	if (typeof(servers) == "string") {
		servers = [ servers ];
	}

	const metrics = await generateMetrics(servers || []);
	res.setHeader("Content-Type", "text/plain");
	res.send(metrics);
});

app.listen(port, () => console.log(`Listening at http://localhost:${port}`))
